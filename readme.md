**Coding test oi**

**Pré requisitos**
 - Java 1.8
 - Maven 3.5 >

**Executando a aplicação com maven**
``sudo mvn spring-boot:run``

**Criando uma imagem Docker com maven**
``sudo mvn install dockerfile:build``

**Rodando do container do docker da solução**
``sudo docker run -p 8080:8080 -t docker-image/desafio-oi``
