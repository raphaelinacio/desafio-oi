FROM openjdk:8u121-jre-alpine
MAINTAINER Raphael Inacio da Silva <contato.raphaelinacio@gmail.com>
VOLUME /tmp
ARG JAR_FILE
ADD ${JAR_FILE} app.jar
EXPOSE 8080
ENTRYPOINT ["sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -jar /app.jar"]
