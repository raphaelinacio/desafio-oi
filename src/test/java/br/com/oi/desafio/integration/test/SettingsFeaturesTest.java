package br.com.oi.desafio.integration.test;

import static org.junit.Assert.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.oi.desafio.DesafioOiApplication;
import br.com.oi.desafio.json.SettingnsJson;
import br.com.oi.desafio.mock.SettingsMock;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@SpringBootTest(classes = DesafioOiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
public class SettingsFeaturesTest {

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private TestRestTemplate restTemplate;

	private ResponseEntity<SettingnsJson> response;

	@Given("^I want to configure the system to perform queries$")
	public void iWantToConfigureTheSystemToPerformQueries() throws Throwable {
	}

	@When("^send a configuration set$")
	public void sendAConfigurationSet(String arg1) throws Throwable {
		response = restTemplate.postForEntity("/api/settings", SettingsMock.create(), SettingnsJson.class);
	}

	@Then("^the response code needs to be (\\d+)$")
	public void theResponseCodeNeedsToBe(int responseCode) throws Throwable {
		assertEquals(responseCode, response.getStatusCode().value());
	}

	@Then("^reply message needs to be Configuração recebida com sucesso$")
	public void replyMessageNeedsToBeConfiguraçãoRecebidaComSucesso() throws Throwable {
		assertEquals("Configuração recebida com sucesso", response.getBody().getMessage());
	}

	@Then("^reply message needs to be Configuração recebida anteriormente$")
	public void replyMessageNeedsToBeConfiguraçãoRecebidaAnteriormente() throws Throwable {
		assertEquals("Configuração recebida anteriormente", response.getBody().getMessage());
	}

}
