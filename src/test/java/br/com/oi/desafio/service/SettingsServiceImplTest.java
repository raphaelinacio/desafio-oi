package br.com.oi.desafio.service;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.oi.desafio.domain.Settings;
import br.com.oi.desafio.json.SettingnsJson;
import br.com.oi.desafio.mock.SettingsMock;
import br.com.oi.desafio.repository.SettingsRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SettingsServiceImplTest {

	@Autowired
	private SettingsService settingService;

	@Autowired
	private SettingsRepository settingsRepository;

	@Before
	public void init() {
		if (!settingsRepository.existsById(1L))
			settingsRepository.save(SettingsMock.create().get());
	}

	@Test
	public void deveSalvarUmaNovaConfiguracao() {
		settingsRepository.deleteAll();
		ResponseEntity<SettingnsJson> response = settingService.saveSettings(SettingsMock.create());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	public void quandoOcorrerUmErroTecnicoDeveRetornar() {
		settingsRepository.deleteAll();
		ResponseEntity<SettingnsJson> response = settingService.saveSettings(SettingsMock.create());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void quandoJaExistirUmaConfiguracaoNaoPodeSalvarORegistro() {
		ResponseEntity<SettingnsJson> response = settingService.saveSettings(SettingsMock.create());
		assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
	}

	@Test
	public void deveAtualizarUmaConfiguracaoExisente() {
		ResponseEntity<SettingnsJson> response = settingService.updateSettings(SettingsMock.create());
		assertEquals(HttpStatus.OK, response.getStatusCode());

	}

	@Test
	public void deveRetornarAsConfiguracoesSalvas() {
		Optional<Settings> mock = SettingsMock.create();
		ResponseEntity<SettingnsJson> response = settingService.getSettings();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(mock.get().getForks(), response.getBody().getSettings().getForks());
		assertEquals(mock.get().getLanguage(), response.getBody().getSettings().getLanguage());
	}
	
	@Test
	public void quandoNaoHouverRegistrosDeveRetornar404() {
		settingsRepository.deleteAll();
		ResponseEntity<SettingnsJson> response = settingService.getSettings();
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

}
