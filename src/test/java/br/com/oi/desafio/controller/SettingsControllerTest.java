package br.com.oi.desafio.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.oi.desafio.domain.Settings;
import br.com.oi.desafio.exeception.SettingsException;
import br.com.oi.desafio.json.SettingnsJson;
import br.com.oi.desafio.mock.SettingsMock;
import br.com.oi.desafio.service.SettingsService;
import br.com.oi.desafio.util.SettingsMessages;

@RunWith(SpringRunner.class)
@WebMvcTest(SettingsController.class)
public class SettingsControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@MockBean
	private SettingsService settingsService;

	@Test
	public void deveGravarConfiguracoesDePesquisaComSucesso() throws Exception {
		Optional<Settings> settings = SettingsMock.create();
		
		when(settingsService.saveSettings(settings)).thenReturn(new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_RECEBIDA_COM_SUCESSO), HttpStatus.OK));
		
		mvc.perform(post("/api/settings")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(settings)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.message").value(SettingsMessages.CONFIGURACAO_RECEBIDA_COM_SUCESSO));
	}
	
	@Test
	public void deveInformarQueJaExistemConfiguracoesGravadas() throws Exception {
		Optional<Settings> settings = SettingsMock.create();
		
		when(settingsService.saveSettings(settings)).thenReturn(new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_RECEBIDA_ANTERIORMENTE), HttpStatus.FORBIDDEN));
		
		mvc.perform(post("/api/settings")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(settings)))
				.andExpect(status().is4xxClientError())
				.andExpect(jsonPath("$.message").value(SettingsMessages.CONFIGURACAO_RECEBIDA_ANTERIORMENTE));
	}
	
	@Test
	public void deveRetornarBadRequestParaErrosGenericos() throws Exception {
		Optional<Settings> settings = SettingsMock.create();
		
		when(settingsService.saveSettings(settings)).thenReturn(new ResponseEntity<>(new SettingnsJson(SettingsException.UNEXPECTED_ERROR), HttpStatus.BAD_REQUEST));
		
		mvc.perform(post("/api/settings")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(settings)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message").value(SettingsException.UNEXPECTED_ERROR));
	}
	
	@Test
	public void deveAtualizarAsConfguracoesComSucesso() throws Exception {
		Optional<Settings> settings = SettingsMock.create();
		
		when(settingsService.updateSettings(settings)).thenReturn(new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_ATUALIZADA_COM_SUCESSO), HttpStatus.OK));
		
		mvc.perform(patch("/api/settings")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(settings)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.message").value(SettingsMessages.CONFIGURACAO_ATUALIZADA_COM_SUCESSO));
	}
	
	@Test
	public void quandoNaoHouverConfiguracoesParaSeremAtualizadasDeveRetoronar404() throws Exception {
		Optional<Settings> settings = SettingsMock.create();
		
		when(settingsService.updateSettings(settings)).thenReturn(new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_NAO_EXISTENTE), HttpStatus.NOT_FOUND));
		
		mvc.perform(patch("/api/settings")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(settings)))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.message").value(SettingsMessages.CONFIGURACAO_NAO_EXISTENTE));
	}
	
	@Test
	public void deveBuscarConfiguracoes() throws Exception {
		Settings mock = SettingsMock.create().get();
		when(settingsService.getSettings()).thenReturn(new ResponseEntity<>(new SettingnsJson(mock), HttpStatus.OK));
		
		mvc.perform(get("/api/settings")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.settings.forks").value(mock.getForks()))
				.andExpect(jsonPath("$.settings.language").value(mock.getLanguage()))
				.andExpect(jsonPath("$.settings.stars.max").value(mock.getStars().getMax()))
				.andExpect(jsonPath("$.settings.stars.min").value(mock.getStars().getMin()));
	}
	
	@Test
	public void deveRetornar404QuandoNaoHouverConfiguracoes() throws Exception {

		when(settingsService.getSettings()).thenReturn(new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_NAO_EXISTENTE), HttpStatus.NOT_FOUND));
		
		mvc.perform(get("/api/settings")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.message").value(SettingsMessages.CONFIGURACAO_NAO_EXISTENTE));
	}

}
