package br.com.oi.desafio.mock;

import java.util.Optional;

import br.com.oi.desafio.domain.Settings;
import br.com.oi.desafio.domain.Stars;

public class SettingsMock {
	public static Optional<Settings> create() {

		Stars stars = new Stars();
		stars.setMax(Long.valueOf(100));
		stars.setMin(Long.valueOf(20));

		Settings settings = new Settings();
		settings.setForks(Long.valueOf(120));
		settings.setLanguage("Java");
		settings.setStars(stars);

		return Optional.of(settings);
	}
}
