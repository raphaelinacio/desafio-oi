Feature: A aplicação deverá expor um RESTful API para receber as configurações e retornar as informações coletadas.

  Scenario Outline: System configuration
    Given I want to configure the system to perform queries
    When  send a configuration set
      """
      { 
      }
      """
    Then the response code needs to be <response-code>
    And reply message needs to be <sucess-message>

    Examples: 
      | response-code | sucess-message                      |
      |           200 | Configuração recebida com sucesso   |
      |           403 | Configuração recebida anteriormente |
