package br.com.oi.desafio.json;

import br.com.oi.desafio.domain.Settings;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SettingnsJson {

	private String message;
	private Settings settings;

	public SettingnsJson(String message) {
		this.message = message;
	}

	public SettingnsJson(String message, Settings settings) {
		this.message = message;
		this.settings = settings;
	}

	public SettingnsJson(Settings settings) {
		this.settings = settings;
	}
}
