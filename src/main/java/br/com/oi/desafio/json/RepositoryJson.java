package br.com.oi.desafio.json;

import java.util.List;

import br.com.oi.desafio.domain.Repository;
import lombok.Data;

@Data
public class RepositoryJson {
	private List<Repository> repositorys;
	private String message;
}
