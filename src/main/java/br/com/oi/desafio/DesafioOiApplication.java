package br.com.oi.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioOiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioOiApplication.class, args);
	}
}
