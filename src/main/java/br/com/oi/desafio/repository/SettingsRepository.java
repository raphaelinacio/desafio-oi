package br.com.oi.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.oi.desafio.domain.Settings;

@Repository
public interface SettingsRepository extends JpaRepository<Settings, Long> {

}
