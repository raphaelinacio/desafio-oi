package br.com.oi.desafio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.oi.desafio.json.RepositoryJson;
import br.com.oi.desafio.service.RepositoryService;

@RestController
@RequestMapping("/api/repository")
public class RepositoryController {
	
	@Autowired
	private RepositoryService repositoryService;
	
	@GetMapping
	public ResponseEntity<RepositoryJson> getRepository() {
		return repositoryService.getRepository();
	}

}
