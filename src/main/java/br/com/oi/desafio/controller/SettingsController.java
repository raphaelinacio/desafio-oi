package br.com.oi.desafio.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.oi.desafio.domain.Settings;
import br.com.oi.desafio.json.SettingnsJson;
import br.com.oi.desafio.service.SettingsService;

@RestController
@RequestMapping("/api/settings")
public class SettingsController {

	private SettingsService settingsService;

	@Autowired
	public SettingsController(SettingsService settingsService) {
		this.settingsService = settingsService;
	}

	@PostMapping
	public @ResponseBody ResponseEntity<SettingnsJson> saveSettings(@RequestBody Optional<Settings> settings) {
		return settingsService.saveSettings(settings);
	}
	
	@PatchMapping
	public @ResponseBody ResponseEntity<SettingnsJson> updateSettings(@RequestBody Optional<Settings> settings) {
		return settingsService.updateSettings(settings);
	}
	
	@GetMapping
	public @ResponseBody ResponseEntity<SettingnsJson> getSettings() {
		return settingsService.getSettings();
	}
}
