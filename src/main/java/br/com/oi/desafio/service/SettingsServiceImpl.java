package br.com.oi.desafio.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.oi.desafio.domain.Settings;
import br.com.oi.desafio.exeception.SettingsException;
import br.com.oi.desafio.json.SettingnsJson;
import br.com.oi.desafio.repository.SettingsRepository;
import br.com.oi.desafio.util.SettingsMessages;

@Service
@Transactional
public class SettingsServiceImpl implements SettingsService {
	
	private static Logger logger = LoggerFactory.getLogger(SettingsServiceImpl.class);

	@Autowired
	private SettingsRepository settingsRepository;

	@Override
	public ResponseEntity<SettingnsJson> saveSettings(Optional<Settings> settings) {

		try {

			logger.info("Iniciando gravação das configurações...");

			if (!settings.isPresent())
				throw new SettingsException();

			Settings newSettings = settings.get();

			if (settingsExists()) {
				logger.info("Já existem configurações gravadas!");
				return new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_RECEBIDA_ANTERIORMENTE), HttpStatus.FORBIDDEN);
			}

			settingsRepository.save(newSettings);
			logger.info("Configurações gravadas com sucesso!");

			return new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_RECEBIDA_COM_SUCESSO), HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Não foi possivel gravar as configurações!");
			return new ResponseEntity<>(new SettingnsJson(SettingsException.UNEXPECTED_ERROR), HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<SettingnsJson> updateSettings(Optional<Settings> settings) {
		
		try {

			logger.info("Iniciando atualiação das configurações...");

			if (!settings.isPresent())
				throw new SettingsException();

			Settings updateSettings = settings.get();

			if (!settingsExists()) {
				logger.info("Não foram encontradas atualizações para serem atualizadas!");
				return new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_NAO_EXISTENTE), HttpStatus.NOT_FOUND);
			}

			settingsRepository.save(updateSettings);
			logger.info("Configurações gravadas com sucesso!");

			return new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_RECEBIDA_COM_SUCESSO), HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Não foi possivel atualizar as configurações!");
			return new ResponseEntity<>(new SettingnsJson(SettingsException.UNEXPECTED_ERROR), HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<SettingnsJson> getSettings() {
		
		try {

			logger.info("Iniciando busca das configurações...");

			if (!settingsExists()) {
				logger.info("Não foram encontradas configuraçoes!");
				return new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_NAO_EXISTENTE), HttpStatus.NOT_FOUND);
			}

			Optional<Settings> settings = settingsRepository.findById(1L);

			if (settings.isPresent())
				return new ResponseEntity<>(new SettingnsJson(settings.get()), HttpStatus.OK);

			return new ResponseEntity<>(new SettingnsJson(SettingsMessages.CONFIGURACAO_NAO_EXISTENTE), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			logger.error("Não foi possivel buscar as configurações!");
			return new ResponseEntity<>(new SettingnsJson(SettingsException.UNEXPECTED_ERROR), HttpStatus.BAD_REQUEST);

		}
	}

	private boolean settingsExists() {
		return settingsRepository.existsById(1L);
	}

}
