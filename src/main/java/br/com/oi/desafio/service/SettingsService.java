package br.com.oi.desafio.service;

import java.util.Optional;

import org.springframework.http.ResponseEntity;

import br.com.oi.desafio.domain.Settings;
import br.com.oi.desafio.json.SettingnsJson;

public interface SettingsService {
	public ResponseEntity<SettingnsJson> saveSettings(Optional<Settings> settings);
	public ResponseEntity<SettingnsJson> updateSettings(Optional<Settings> settings);
	public ResponseEntity<SettingnsJson> getSettings();
}
