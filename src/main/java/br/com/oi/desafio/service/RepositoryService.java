package br.com.oi.desafio.service;

import org.springframework.http.ResponseEntity;

import br.com.oi.desafio.json.RepositoryJson;

public interface RepositoryService {
	ResponseEntity<RepositoryJson> getRepository();
}
