package br.com.oi.desafio.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class Settings {
	@Id
	private final Long id = 1L;
	private String language;
	@OneToOne(cascade = CascadeType.ALL)
	private Stars stars;
	private Long forks;
}
