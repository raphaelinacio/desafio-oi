package br.com.oi.desafio.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Stars {
	@Id
	@GeneratedValue
	private Long id;
	private Long min;
	private Long max;
}
