package br.com.oi.desafio.exeception;

public class SettingsException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String UNEXPECTED_ERROR = "Unexpected error";

	public SettingsException() {
		super(UNEXPECTED_ERROR);
	}
}
