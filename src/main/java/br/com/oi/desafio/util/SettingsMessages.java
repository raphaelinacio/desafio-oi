package br.com.oi.desafio.util;

public class SettingsMessages {
	public  static final String CONFIGURACAO_NAO_EXISTENTE = "Configuração não existente";
	public static final String CONFIGURACAO_RECEBIDA_COM_SUCESSO = "Configuração recebida com sucesso";
	public static final String CONFIGURACAO_ATUALIZADA_COM_SUCESSO = "Configuração atualizada com sucesso";
	public static final String CONFIGURACAO_RECEBIDA_ANTERIORMENTE = "Configuração recebida anteriormente";
}
